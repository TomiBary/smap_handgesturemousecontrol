﻿using System.Collections.Generic;
using Mediapipe;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using static HandHelpers.HandConnections;
using static HandHelpers.HandPoints;
using static MouseController;

public class MainController : MonoBehaviour {
	public int port = 8080;
	public bool enableClicking;
	public bool enableMoving;
	public bool enableBasicClick;
	[Tooltip("How close fingers must be to perform click")]
	public float clickTriggerDistance;
	[Tooltip("Size of landmark point")]
	public float pointScale = 1;
	[Tooltip("Offset for mouse access to screen corners")]
	[Range(0, 1)]
	public float frameOffset = 0.8f;
	[Tooltip("When should mouse update position - when difference from last position is bigger than value")]
	[Range(0, 0.5f)]
	public float mouseUpdatePosDiff = 0.01f;
	[Tooltip("How much last positions should be used for smoothing movement of the cursor")]
	public int smoothingCount = 20;
	
	public Text mousePositionText;
	public Transform handPivot;
	public LineRenderer lineRendererPrefab;
	public Material pointMaterial, markMaterial;
    
	private float clickRate = 1f;
	private float lastClick;
	
	private UdpManager _udpManager;
	private List<LineRenderer> lineRendererList;
	private List<GameObject> landmarkGOList;
	private FixedSizedQueue<Vector2> mousePositionQueue;
	private Vector2 lastPosition;
	private bool isMouseDown;

	//DONE nastavit okraje pro přístup myši až do rohů
	//DONE Stabilizovat pohyb myši,
	//DONE označit prst/bod značící cursor
	//DONE běží v pozadí
	//TODO Optional: Průhledná appka po buildu

	void Start() {
		Application.runInBackground = true;
		
		_udpManager = new UdpManager(port);
		mousePositionQueue = new FixedSizedQueue<Vector2>(smoothingCount);

		lineRendererList = new List<LineRenderer>();
		for (int i = 0; i < 4; i++) {
			var lineRenderer = Instantiate(lineRendererPrefab, handPivot);
			lineRenderer.gameObject.name = $"LineRenderer{i}";
			lineRenderer.positionCount = pointCountList[i];
			lineRendererList.Add(lineRenderer);
		}
        landmarkGOList = new List<GameObject>(21);
		for (var i = 0; i < 21; i++) {
			var landmarkGO = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			landmarkGO.GetComponent<MeshRenderer>().sharedMaterial = i == 8 ? markMaterial : pointMaterial;
			landmarkGO.transform.localScale = Vector3.one * pointScale;
			landmarkGO.transform.parent = handPivot;
			landmarkGOList.Add(landmarkGO);
		}
		Debug.Log(Screen.currentResolution);
	}

	private void Update() {
		_udpManager.Receive(ProcessLandmarks);

		if (Keyboard.current.gKey.wasPressedThisFrame)
			enableMoving = !enableMoving;

		if (Keyboard.current.fKey.wasPressedThisFrame)
			enableClicking = !enableClicking;
	}
	
	private void ProcessLandmarks(LandmarkList landmarkList) {
		if (landmarkList.Landmark.Count != 21)
			Debug.LogError("Fu*k, i got invalid count of landmarks, this should not happen!");
		var transformedLandmarks = TransformLandmarks(landmarkList);

		UpdateUIPoints(transformedLandmarks);
		UpdateConnections(transformedLandmarks);
		if (enableMoving) {
			UpdateMousePosition(transformedLandmarks[(int)INDEX_FINGER_TIP]);
		}
		if (enableClicking) {
			ProcessClick(transformedLandmarks);
		}
    }

	private static List<Vector3> TransformLandmarks(LandmarkList landmarkList) {
		List<Vector3> transformedLandmarks = new List<Vector3>(21);
		foreach (var l in landmarkList.Landmark) {
			//from [0, 1] to [-0.5, 0.5] (z axis can be ignored - mediapipe docs)
			var position = l.ToVec3() - (Vector3.one.WithZ(0) / 2);
			//Must be reassigned or directly returned (info in extension methods like WithX())
			transformedLandmarks.Add(position.WithZ(-position.z).WithY(-position.y)); // invert axis
		}
		return transformedLandmarks;
	}

	private void UpdateUIPoints(List<Vector3> landmarkList) {
		for (var i = 0; i < landmarkList.Count; i++) {
			landmarkGOList[i].transform.localPosition = landmarkList[i];
			landmarkGOList[i].transform.localScale = Vector3.one * pointScale;
		}
	}

	private void UpdateConnections(List<Vector3> landmarkList) {
		for (var i = 0; i < connectionList.Count; i++) {
			var pointsArray = connectionList[i];
			var lr = lineRendererList[i];
			for (int j = 0; j < pointsArray.Length; j++) {
				var pos = handPivot.position;
				var landmark = pos + landmarkList[(int)pointsArray[j]];
				lr.SetPosition(j, landmark.RotatePointAroundPivot(pos, handPivot.eulerAngles));
			}
		}
	}

	private void UpdateMousePosition(Vector3 indexFingerTipPosition) {
		//Transform to 0-1 from -0,5-0,5
		var vec3 = indexFingerTipPosition + Vector3.one / 2;
		var vec2 = vec3.WithY(1 - vec3.y).XY();
		
		var pos  = SmoothMousePosition(vec2);
		
		if (Vector2.Distance(lastPosition, vec2) < mouseUpdatePosDiff)
			return;
		lastPosition = pos;

		//Offset transformation for cursor access to corners (0-1 -> 0,2 = 0% | 0,8 = 100%)
		var offset = 1 - frameOffset;
		pos = pos * (1 + 2 * offset) - Vector2.one * offset;
		var resolution = Screen.currentResolution;
		pos *= new Vector2(resolution.width, resolution.height);
		
		mousePositionText.text = $"Position: {pos}:{indexFingerTipPosition}";
        SetCursorPosition((int)pos.x, (int)pos.y);
    }
	
	private Vector2 SmoothMousePosition(Vector2 newValue) {
		mousePositionQueue.Enqueue(newValue);
		return mousePositionQueue.Average();
	}

	private void ProcessClick(List<Vector3> transformedLandmarks) {
		var distance = Vector3.Distance(transformedLandmarks[(int)THUMB_TIP], transformedLandmarks[(int)INDEX_FINGER_PIP]);

		if (distance <= clickTriggerDistance) {
			if (enableBasicClick && Time.time > clickRate + lastClick)
			{
				Debug.Log("Click");
				LeftClickWithDelay(200);
				lastClick = Time.time;
			}

			if (enableBasicClick || isMouseDown) return;
			Debug.Log("Mouse pressed!");
			LeftMouseDown();
			isMouseDown = true;
		}
		else {
			if (enableBasicClick || !isMouseDown) return;
			Debug.Log("Mouse released!");
			LeftMouseUp();
			isMouseDown = false;
		}
		
		
        
    }

    
    
    
}