﻿using System.Collections.Generic;
using static HandHelpers.HandPoints;

public static class HandHelpers {
	public enum HandPoints {
		WRIST = 0,
		THUMB_CMC = 1,
		THUMB_MCP = 2,
		THUMB_IP = 3,
		THUMB_TIP = 4,
		INDEX_FINGER_MCP = 5,
		INDEX_FINGER_PIP = 6,
		INDEX_FINGER_DIP = 7,
		INDEX_FINGER_TIP = 8,
		MIDDLE_FINGER_MCP = 9,
		MIDDLE_FINGER_PIP = 10,
		MIDDLE_FINGER_DIP = 11,
		MIDDLE_FINGER_TIP = 12,
		RING_FINGER_MCP = 13,
		RING_FINGER_PIP = 14,
		RING_FINGER_DIP = 15,
		RING_FINGER_TIP = 16,
		PINKY_MCP = 17,
		PINKY_PIP = 18,
		PINKY_DIP = 19,
		PINKY_TIP = 20
	}

	public struct HandConnections {
		private static readonly HandPoints[] First = {
			THUMB_TIP,
			THUMB_IP,
			THUMB_MCP,
			THUMB_CMC,
			WRIST,
			PINKY_MCP,
			PINKY_PIP,
			PINKY_DIP,
			PINKY_TIP
		};
		private static readonly HandPoints[] Second = {
			INDEX_FINGER_TIP,
			INDEX_FINGER_DIP,
			INDEX_FINGER_PIP,
			INDEX_FINGER_MCP,
			WRIST,
			RING_FINGER_MCP,
			RING_FINGER_PIP,
			RING_FINGER_DIP,
			RING_FINGER_TIP
		};
		private static readonly HandPoints[] Third = {
			WRIST,
			MIDDLE_FINGER_MCP,
			MIDDLE_FINGER_PIP,
			MIDDLE_FINGER_DIP,
			MIDDLE_FINGER_TIP
		};
		private static readonly HandPoints[] Forth = {
			THUMB_MCP,
			THUMB_CMC,
			WRIST,
			PINKY_MCP,
			RING_FINGER_MCP,
			MIDDLE_FINGER_MCP,
			INDEX_FINGER_MCP,
			THUMB_MCP
		};

		public static readonly List<int> pointCountList = new List<int> {9, 9, 5, 8};
		public static readonly List<HandPoints[]> connectionList = new List<HandPoints[]> { First, Second, Third, Forth };
	}
}