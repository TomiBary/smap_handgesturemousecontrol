using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Server : MonoBehaviour
{
//     private const int Port = 6789;
//     private const float Timeout = 30f; // seconds
//     private const float KeepAliveFrequency = 1f; // seconds

//     private static int _idCounter;

//     private SkinnedMeshRenderer[] renderers;

//     private enum TransportType
//     {
//         Connectionless = 0,
//         Unreliable = 1,
//         Sequenced = 2,
//         Reliable = 3
//     }

//     private enum MessageType
//     {
//         Discovery = 0,
//         Connect = 1,
//         Disconnect = 2,
//         BlendshapeData = 3,
//         ApplicationMessage = 4,
//         KeepAlive = 5,
//         BlendshapeNames = 6
//     }

//     private UdpClient _client;

//     private float counter = 0.0f;

//     private Vector3 _position = Vector3.zero, _scale = Vector3.one;
//     private Quaternion _rotation = Quaternion.identity;


//     private class UdpConnection
//     {
//         public float LastTransmission;
//         public float LastOutgoingTransmission;
//         public IPEndPoint EndPoint;
//         public readonly int Id = _idCounter++;
//         public byte LastReceiveSequence, LastSendSequence;
//         public byte LastReceiveReliable, LastSendReliable;
//     }

//     private readonly Dictionary<IPEndPoint, UdpConnection> _connections =
//         new Dictionary<IPEndPoint, UdpConnection>();


//     private void Start()
//     {
//         var receivingEndPoint = new IPEndPoint(IPAddress.Any, Port);
//         _client = new UdpClient(receivingEndPoint);
//     }

//     private void Update()
//     {
//         Receive();
//         // CheckTimeouts();
//         // SendKeepAlives();


//         counter += 1.0f;

//         if (counter > 100f)
//         {
//             counter = 0.0f;
//         }
//     }

//     private void Receive()
//     {
//         while (_client.Available > 0)
//         {

//             var endPoint = new IPEndPoint(IPAddress.Any, Port);
//             byte[] data;
//             try
//             {
//                 data = _client.Receive(ref endPoint);
//             }
//             catch (ObjectDisposedException)
//             {
//                 Disconnect(endPoint);
//                 continue;
//             }
//             catch (SocketException ex)
//             {
//                 if (_connections.ContainsKey(endPoint))
//                 {
//                     Disconnect(endPoint);
//                     Debug.LogError("UDP socket exception: \n" + ex);
//                 }

//                 continue;
//             }

//             var connection = AssignConnection(endPoint);

//             //TODO process data


//             // var transportType = (int)data[0];

//             // switch ((TransportType)transportType)
//             // {
//             //     case TransportType.Connectionless:
//             //         ProcessConnectionless(endPoint, data);
//             //         break;
//             //     case TransportType.Unreliable:
//             //         ProcessUnreliable(connection, data);
//             //         break;
//             //     case TransportType.Sequenced:
//             //         ProcessSequenced(connection, data);
//             //         break;
//             //     case TransportType.Reliable:
//             //         throw new NotImplementedException();
//             //     default:
//             //         Debug.Log("Unknown transport type " + transportType);
//             //         break;
//             // }
//         }
//     }

//     private void Connect(IPEndPoint endPoint)
//     {
//         var newConnection = new UdpConnection
//         {
//             LastTransmission = Time.time,
//             EndPoint = endPoint
//         };
//         _connections[endPoint] = newConnection;
//     }

//     private UdpConnection AssignConnection(IPEndPoint endPoint)
//     {
//         return !_connections.ContainsKey(endPoint) ? null : _connections[endPoint];
//     }

//     private void Disconnect(IPEndPoint endPoint)
//     {
//         if (!_connections.ContainsKey(endPoint)) return;

//         Debug.Log("Client " + endPoint + " disconnected");

//         _connections.Remove(endPoint);
//     }


//     // private void CheckTimeouts()
//     // {
//     //     var time = Time.time;
//     //     var toDisconnect = new List<IPEndPoint>();
//     //     foreach (var connection in _connections)
//     //     {
//     //         if (time - connection.Value.LastTransmission > Timeout)
//     //         {
//     //             toDisconnect.Add(connection.Key);
//     //         }
//     //     }

//     //     toDisconnect.ForEach(Disconnect);
//     // }

//     // private void SendKeepAlives()
//     // {
//     //     var time = Time.time;
//     //     foreach (var connection in _connections)
//     //     {
//     //         if (time - connection.Value.LastOutgoingTransmission > KeepAliveFrequency)
//     //         {
//     //             SendKeepalive(connection.Value.EndPoint);
//     //             connection.Value.LastOutgoingTransmission = time;
//     //         }
//     //     }
//     // }

//     private static byte[] SubArray(byte[] source, int from, int to = -1)
//     {
//         if (to < 0)
//             to = source.Length;
//         var result = new byte[to - from];
//         Array.Copy(source, from, result, 0, to - from);
//         return result;
//     }

//     private void SendTo(IPEndPoint endPoint, byte[] data)
//     {
//         _client.SendAsync(data, data.Length, endPoint);
//     }

//     private void SendDiscoveryResponse(IPEndPoint endPoint)
//     {
//         var nameBytes = Encoding.Default.GetBytes(Environment.MachineName);
//         var result = new byte[2 + nameBytes.Length + 1];
//         result[0] = (byte)TransportType.Connectionless;
//         result[1] = (byte)MessageType.Discovery;
//         Array.Copy(nameBytes, 0, result, 2, nameBytes.Length);
//         result[result.Length - 1] = 0;
//         SendTo(endPoint, result);
//     }

//     private void SendConnectResponse(IPEndPoint endPoint)
//     {
//         var result = new byte[2];
//         result[0] = (byte)TransportType.Connectionless;
//         result[1] = (byte)MessageType.Connect;
//         SendTo(endPoint, result);
//     }

//     private void SendKeepalive(IPEndPoint endPoint)
//     {
//         var result = new byte[2];
//         result[0] = (byte)TransportType.Unreliable;
//         result[1] = (byte)MessageType.KeepAlive;
//         SendTo(endPoint, result);
//     }

//     private void ProcessConnectionless(IPEndPoint endPoint, byte[] data)
//     {
//         var messageType = data[1];
//         switch ((MessageType)messageType)
//         {
//             case MessageType.Discovery:
//                 SendDiscoveryResponse(endPoint);
//                 break;
//             case MessageType.Connect:
//                 Connect(endPoint);
//                 SendConnectResponse(endPoint);
//                 break;
//             default:
//                 Debug.Log("Unknown message " + messageType);
//                 break;
//         }
//     }

//     private void ProcessUnreliable(UdpConnection connection, byte[] data)
//     {
//         if (connection == null) // client trying to send data before connecting
//             return;
//         byte messageType = data[1];
//         DispatchData(connection, SubArray(data, 2), messageType);
//     }

//     private void ProcessSequenced(UdpConnection connection, byte[] data)
//     {
//         if (connection == null) // client trying to send data before connecting
//             return;
//         var sequence = data[1];
//         var messageType = data[2];

//         if (sequence < connection.LastReceiveSequence)
//         {
//             if (connection.LastReceiveSequence <= 250 || sequence >= 5) // overflow check
//                 return; // no overflow, received old packet, discard
//         }

//         connection.LastReceiveSequence = sequence;

//         DispatchData(connection, SubArray(data, 3), messageType);
//     }

//     private void DispatchData(UdpConnection connection, byte[] data, int messageType)
//     {
//         connection.LastTransmission = Time.time;

//         switch ((MessageType)messageType)
//         {
//             case MessageType.BlendshapeData:

//                 break;
//             case MessageType.BlendshapeNames:
//                 int bsCount = data[0];
//                 var offset = 1;
//                 var names = new string[bsCount];
//                 for (var i = 0; i < bsCount; i++)
//                 {
//                     int len = data[offset];
//                     offset++;
//                     var n = Encoding.Default.GetString(data, offset, len);
//                     offset += len;
//                     names[i] = n;
//                 }

//                 blendshapeKeys = names;
//                 break;
//             case MessageType.KeepAlive:
//                 break;
//             case MessageType.Disconnect:
//                 Disconnect(connection.EndPoint);
//                 break;
//             default:
//                 Debug.Log("Unknown message type " + messageType);
//                 break;
//         }
//     }
}