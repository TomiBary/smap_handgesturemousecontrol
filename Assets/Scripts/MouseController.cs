using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

public static class MouseController {

	public static void LeftMouseDown() {
		Windows.MouseEvent(Windows.MouseEventFlags.LeftDown);
	}
	
	public static void LeftMouseUp() {
		Windows.MouseEvent(GetCursorPosition(), Windows.MouseEventFlags.LeftUp);
	}
	public static async void LeftClickWithDelay(int millisDelay) {
		var cursorPosition = GetCursorPosition();
		Windows.MouseEvent(cursorPosition, Windows.MouseEventFlags.LeftDown);
		await Task.Run(async () => {
			await Task.Delay(millisDelay);
			Windows.MouseEvent(cursorPosition, Windows.MouseEventFlags.LeftUp);
		});
	}
	
	public static void SetCursorPosition(int x, int y) {
		Windows.SetCursorPos(x, y);
	}

	public static void SetCursorPosition(Windows.MousePoint point) {
		Windows.SetCursorPos(point.X, point.Y);
	}
	
	public static Windows.MousePoint GetCursorPosition() {
		Windows.MousePoint currentMousePoint;
		var gotPoint = Windows.GetCursorPos(out currentMousePoint);
		if (!gotPoint) {
			currentMousePoint = new Windows.MousePoint(0, 0);
		}

		return currentMousePoint;
	}
	
	public static class Windows {
		[Flags]
		public enum MouseEventFlags {
			LeftDown = 0x00000002,
			LeftUp = 0x00000004,
			MiddleDown = 0x00000020,
			MiddleUp = 0x00000040,
			Move = 0x00000001,
			Absolute = 0x00008000,
			RightDown = 0x00000008,
			RightUp = 0x00000010
		}
		
		public static void MouseEvent(MouseEventFlags value) {
			MousePoint position = GetCursorPosition();

			mouse_event
			((int) value,
				position.X,
				position.Y,
				0,
				0);
		}
		
		public static void MouseEvent(MousePoint position, MouseEventFlags value) {
			mouse_event((int) value, position.X, position.Y, 0, 0);
		}

		[DllImport("user32.dll", EntryPoint = "SetCursorPos")]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool SetCursorPos(int x, int y);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool GetCursorPos(out MousePoint lpMousePoint);

		[DllImport("user32.dll")]
		private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

		[StructLayout(LayoutKind.Sequential)]
		public struct MousePoint {
			public int X;
			public int Y;

			public MousePoint(int x, int y) {
				X = x;
				Y = y;
			}
		}
	}
}