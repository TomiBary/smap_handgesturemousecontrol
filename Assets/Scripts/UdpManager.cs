﻿using System;
using System.Net;
using System.Net.Sockets;
using Mediapipe;
using UnityEngine;

public class UdpManager
{
    private readonly UdpClient client;
    private readonly int serverPort;

    public UdpManager(int serverPort) {
        this.serverPort = serverPort;
        client = new UdpClient(serverPort) {Client = {Blocking = false}};
    }

    public void Receive(Action<LandmarkList> landmarkFunc)
    {
        while (client.Available > 0)
        {
            var endPoint = new IPEndPoint(IPAddress.Any, serverPort);
            byte[] data;
            try
            {
                data = client.Receive(ref endPoint);
                LandmarkList landmarkList = LandmarkList.Parser.ParseFrom(data);
                landmarkFunc?.Invoke(landmarkList);
            }
            catch (Exception ex)
            {
                Debug.LogError("UDP socket exception: \n" + ex);
            }
        }
    }
}
