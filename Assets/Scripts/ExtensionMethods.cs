﻿using System.Collections.Generic;
using System.Linq;
using Mediapipe;
using UnityEngine;

public static class ExtensionMethods {
	public static Vector3 ToVec3(this Landmark landmark) {
		return new Vector3(landmark.X, landmark.Y, landmark.Z);
	}

	public static Vector2 Average(this IEnumerable<Vector2> source) {
		if (!source.Any())
			return Vector2.zero;
		var sum = source.Aggregate((s1, s2) => s1 + s2);
		var count = source.Count();
		return sum / new Vector2(count, count);
	}

	public static Vector3 RotatePointAroundPivot(this Vector3 point, Vector3 pivot, Vector3 angles) {
		Vector3 dir = point - pivot; // get point direction relative to pivot
		dir = Quaternion.Euler(angles) * dir; // rotate it
		return dir + pivot; // calculate rotated point and return it
	}

	public static Vector2 XY(this Vector3 v) {
		return new Vector2(v.x, v.y);
	}
		
	//IMPORTANT Cant just modified existing instance -
	// after applying WithX it should be assigned to variable or it wont work
	// methods below are correct bcs they return new instance
	public static Vector3 WithX(this Vector3 v, float x) {
		return new Vector3(x, v.y, v.z);
	}

	public static Vector3 WithY(this Vector3 v, float y) {
		return new Vector3(v.x, y, v.z);
	}

	public static Vector3 WithZ(this Vector3 v, float z) {
		return new Vector3(v.x, v.y, z);
	}

	public static Vector2 NewWithX(this Vector2 v, float x) {
		return new Vector2(x, v.y);
	}
	
	public static Vector2 NewWithY(this Vector2 v, float y) {
		return new Vector2(v.x, y);
	}
	
	public static Vector3 NewWithZ(this Vector2 v, float z) {
		return new Vector3(v.x, v.y, z);
	}
	
	public static Vector3 ToVec3(this Vector2 v) {
		return new Vector3(v.x, v.y, 0);
	}
	
	public static Vector3 ToVec3(this Vector2 v, float z) {
		return new Vector3(v.x, v.y, z);
	}

	public static Vector3 GetMulVector3(this Vector3 v1, Vector3 v2) {
		return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
	}
}